<?php

namespace Drupal\rosreklama_pages\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
* Provides route responses for the jobs page.
*/
class JobsPageController extends ControllerBase {
  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * @return array
   */
  public function build() {
    $jobs = $this->getJobs();

    return [
      '#theme' => 'jobs_page',
      '#data' => $jobs,
    ];
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getJobs() {
    return $this->entityTypeManager->getStorage('node')->loadByProperties([
      'type' => 'job',
      'status' => 1,
    ]);
  }

}
