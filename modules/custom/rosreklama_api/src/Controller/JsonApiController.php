<?php

namespace Drupal\rosreklama_api\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides route responses for the rosreklama maps module.
 */
class JsonApiController extends ControllerBase {

  public function placesApi() {
    $token = \Drupal::request()->request->get('token');
    $item = \Drupal::request()->request->get('item');
    $courier_id = LoginApiController::mc_decrypt($token);
    $sorting = \Drupal::request()->request->get('sorting');
    $street = \Drupal::request()->request->get('street');

    if(!$token && !$courier_id) {
      return new JsonResponse([
        'result' => 0,
        'error' => 'Не совпадают токены',
      ], 200, ['Content-Type'=> 'application/json']);
    }

    if ($item && !is_numeric($item)) {
      return new JsonResponse([
        'result' => 0,
        'error' => 'Смещение должно быть числом',
      ], 200, ['Content-Type'=> 'application/json']);
    }

    if ($sorting && ($sorting != 'ASC' && $sorting != 'DESC')) {
      return new JsonResponse([
        'result' => 0,
        'error' => 'Wrong order rule',
      ], 200, ['Content-Type'=> 'application/json']);
    }

    $street = Html::escape($street);

    return $this->getResults($courier_id, $street, $sorting, $item);
  }

  public function getResults($courier_id, $street = NULL, $sorting = 'ASC', $item = 0) {
    if($courier_id) {
      $query = \Drupal::database()->select('profile', 'p');
      $query->leftjoin('profile__field_adresa', 'a', 'a.entity_id = p.profile_id');
      $query->leftjoin('node__field_market_place_address_text', 'n', 'n.entity_id = a.field_adresa_target_id');
      $query->fields('a', ['field_adresa_target_id']);
      $query->condition('p.uid', $courier_id);
      $query->condition('p.type', 'courier');
      if ($street) {
        $query->condition('field_market_place_address_text_value', '%' . db_like($street) . '%', 'LIKE');
      }
      $query->orderBy('n.field_market_place_address_text_value', $sorting);
      $query->range($item, 29);
      $nids = $query->execute()->fetchCol();

      if(empty($nids)) {
        return new JsonResponse([
          'result' => 0,
          'error' => $this->t('No streets found for courier with id @id.', [
            '@id' => $courier_id,
          ]),
        ], 200, ['Content-Type'=> 'application/json']);
      }
    }
    else {
      return new JsonResponse([
        'result' => 0,
        'error' => 'Пользователя не существует',
      ], 200, ['Content-Type'=> 'application/json']);
    }
    $nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);
    $result = [];
    $curent_date = time();
    foreach ($nodes as $i => $item) {
      $boxes = [];

      $paragraph_reference = $item->field_market_place_photodata->getValue();

      foreach ($paragraph_reference as $k => $paragraph) {
        $boxes = [];
        $market_box = \Drupal\paragraphs\Entity\Paragraph::load($paragraph['target_id']);
        $old = TRUE;

        if (!$market_box->field_market_box_image->isEmpty()) {
          $image = $market_box->field_market_box_image->getValue();
          $image = \Drupal\file\Entity\File::load($image[0]['target_id']);
          $image_create = (int)$image->created->getValue()[0]['value'];
          $seconds = abs($curent_date - $image_create);
          $days = floor($seconds / 86400);
          if($days < 3) {
            $old = FALSE;
          }
        }

        if (!$market_box->field_market_box_id->isEmpty()) {
          $field_box_id = $market_box->field_market_box_id->getValue()[0]['value'];
        }

        $boxes[] = [
          'box_id' => $field_box_id,
          'old' => $old,
        ];

        $place_text_arr = [
          'entrance' => 'подъезд',
          'floor' => 'этаж',
          'structure' => 'строение',
          'building' => 'корпус',
        ];

        $address = $item->get('field_market_place_address_text')->value . ', ';
        $address .= $place_text_arr[$market_box->field_market_box_place_text->value] . ' ';
        $address .= $market_box->field_market_box_place_int->value;

        $result[] = [
          'address' => $address,
          'boxes' => $boxes,
        ];
      }
    }
    return new JsonResponse([
      'result' => 1,
      'data' => $result,
    ], 200, ['Content-Type'=> 'application/json']);
  }
}
