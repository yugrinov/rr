<?php

  namespace Drupal\rosreklama_api\Controller;

  use Drupal\Core\Controller\ControllerBase;

  use Drupal\Core\Database\Connection;
  use Drupal\Core\Entity\EntityTypeManagerInterface;
  use Drupal\Core\StringTranslation\StringTranslationTrait;
  use Drupal\paragraphs\Entity\Paragraph;
  use Drupal\paragraphs\ParagraphInterface;
  use Symfony\Component\DependencyInjection\ContainerInterface;
  use Symfony\Component\HttpFoundation\JsonResponse;
  use Symfony\Component\HttpFoundation\Request;
  use Symfony\Component\HttpKernel\Exception\HttpException;

  /**
   * Class Box
   *
   * @package Drupal\rosreklama_api\Controller
   */
  class Box extends ControllerBase {

    use StringTranslationTrait;

    /**
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * The current request.
     *
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $currentRequest;

    /**
     * @var \Drupal\Core\Database\Connection
     */
    protected $database;

    /**
     * Box constructor.
     *
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
     * @param \Symfony\Component\HttpFoundation\Request $current_request
     * @param \Drupal\Core\Database\Connection $connection
     */
    public function __construct(EntityTypeManagerInterface $entityTypeManager, Request $current_request, Connection $connection) {
      $this->entityTypeManager = $entityTypeManager;
      $this->currentRequest = $current_request;
      $this->database = $connection;
    }

    public static function create(ContainerInterface $container) {
      return new static(
        $container->get('entity_type.manager'),
        $container->get('request_stack')->getCurrentRequest(),
        $container->get('database')
      );
    }

    public function edit() {
      $token = \Drupal::request()->request->get('token');
      $box_id = \Drupal::request()->request->get('box_id');
      $images = $this->currentRequest->files;
      $coords = \Drupal::request()->request->get('coords');

      $courier_id = LoginApiController::mc_decrypt($token);
      if(!$token && !$courier_id) {
        return new JsonResponse([
          'result' => 0,
          'message' => $this->t('Tokens do not match.'),
        ], 200, ['Content-Type'=> 'application/json']);
      }

      if ($images->count() == 0) {
        return new JsonResponse([
          'result' => 0,
          'message' => $this->t('Images not attached.'),
        ], 200, ['Content-Type'=> 'application/json']);
      }

      $keys = $images->keys();
      /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploaded_file */
      $uploaded_file = $this->currentRequest->files->get($keys[0]);
      $file_name = $uploaded_file->getClientOriginalName();
      $file_extension = $uploaded_file->getClientOriginalExtension();
      if (empty($file_name)) {
        throw new HttpException(400, 'File name not found');
      }

      $path = $uploaded_file->getPathName();
      $file_data = file_get_contents($path, FILE_USE_INCLUDE_PATH);

      if (FALSE === $file_data) {
        throw new HttpException(400, 'File could not be processed');
      }

      return $this->box_edit($courier_id, $box_id, $file_data, $file_name, $file_extension, $coords);
    }

    public function box_edit($courier_id, $box_id, $file_data, $file_name, $file_extension, $coords) {
      if ($courier_id) {
        $query = $this->database->select('paragraph__field_market_box_id', 'box');
        $query->condition('box.field_market_box_id_value', $box_id);
        $query->addField('box', 'entity_id');

        $paragraph_id = $query->execute()->fetchField();

        if (!$paragraph_id) {
          return new JsonResponse([
            'result' => 0,
            'message' => $this->t('Box with id @box_id does not exist.', [
              '@id' => $box_id,
            ]),
          ], 200, ['Content-Type' => 'application/json']);
        }

        $paragraph = Paragraph::load($paragraph_id);
        if (!$paragraph) {
          return new JsonResponse([
            'result' => 0,
            'message' => $this->t('Can not load box with @id: ', [
              '@id' => $box_id,
            ]),
          ], 200, ['Content-Type' => 'application/json']);
        }

        $file_saved = file_save_data($file_data, 'public://' . $file_name, 'FILE_EXISTS_REPLACE');
        $fid = $file_saved->id();

        if ($paragraph instanceof ParagraphInterface) {
          try {
            $paragraph->set('field_market_box_image', $fid);
            $paragraph->save();

            return new JsonResponse([
              'result' => 1,
              'message' => $this->t('Successfully update image for box with @id', [
                '@id' => $box_id,
              ]),
            ], 200, ['Content-Type' => 'application/json']);
          } catch (\Exception $e) {
            watchdog_exception('Error when update image', $e);
            return new JsonResponse([
              'result' => 0,
              'message' => $this->t('Error when update image for box with @id: ', [
                '@id' => $box_id,
              ]),
            ], 200, ['Content-Type' => 'application/json']);
          }
        }

      }
      else {
        return new JsonResponse([
          'result' => 0,
          'message' => $this->t('Coruier with id @id does not exist.', [
            '@id' => $courier_id,
          ]),
        ], 200, ['Content-Type' => 'application/json']);
      }
    }

  }
