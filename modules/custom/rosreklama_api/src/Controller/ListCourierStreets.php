<?php

namespace Drupal\rosreklama_api\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ListCourierStreets
 *
 * @package Drupal\rosreklama_api\Controller
 */
class ListCourierStreets extends ControllerBase {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  public function listStreet() {
    $token = \Drupal::request()->request->get('token');
    $start = \Drupal::request()->request->get('item');
    \Drupal::logger('Objects')->notice('token:' . $token);
    $courier_id = LoginApiController::mc_decrypt($token);
    $sorting = \Drupal::request()->request->get('sorting');
    $street = \Drupal::request()->request->get('street');

    if(!$token && !$courier_id) {
      return new JsonResponse([
        'result' => 0,
        'error' => $this->t('Tokens do not match.'),
      ], 200, ['Content-Type'=> 'application/json']);
    }

    if ($sorting && ($sorting != 'ASC' && $sorting != 'DESC')) {
      return new JsonResponse([
        'result' => 0,
        'error' => 'Wrong order rule',
      ], 200, ['Content-Type'=> 'application/json']);
    }

    return $this->getStreet($courier_id, $start, $sorting, $street);
  }

  public function getStreet($courier_id, $start = 0, $sorting = 'ASC', $street = NULL) {
    \Drupal::logger('Objects')->notice('Account_id:' . $courier_id);

    if ($courier_id) {
      $query = \Drupal::database()->select('profile', 'p');
      $query->leftjoin('profile__field_adresa', 'a', 'a.entity_id = p.profile_id');
      $query->leftjoin('node__field_market_place_street', 'street', 'street.entity_id = a.field_adresa_target_id');
      $query->fields('a', ['field_adresa_target_id']);
      $query->condition('p.uid', $courier_id);
      $query->condition('p.type', 'courier');
      if ($street) {
        $query->condition('street.field_market_place_street_value', '%' . db_like($street) . '%', 'LIKE');
      }
      $query->orderBy('street.field_market_place_street_value', $sorting);
      $query->range($start, 29);

      $ids = $query->execute()->fetchCol();

      if(empty($ids)) {
        return new JsonResponse([
          'result' => 0,
          'error' => $this->t('No streets found for courier with id @id.', [
            '@id' => $courier_id,
          ]),
        ], 200, ['Content-Type'=> 'application/json']);
      }
    }
    else {
      return new JsonResponse([
        'result' => 0,
        'error' => $this->t('Coruier with id @id does not exist.', [
          '@id' => $courier_id,
        ]),
      ], 200, ['Content-Type'=> 'application/json']);
    }

    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($ids);

    $streets = [];

    foreach ($nodes as $node) {
      if (!$node->field_market_place_street->isEmpty()) {
        $street_name = $node->field_market_place_street->value;
        if (!in_array($street_name, $streets)) {
          $streets[] = $street_name;
        }
      }
    }

    return new JsonResponse([
      'result' => 1,
      'data' => $streets,
    ], 200, ['Content-Type'=> 'application/json']);
  }
}
