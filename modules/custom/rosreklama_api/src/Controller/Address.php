<?php

  namespace Drupal\rosreklama_api\Controller;

  use Drupal\Core\Controller\ControllerBase;

  use Drupal\Core\Database\Connection;
  use Drupal\Core\Entity\EntityTypeManagerInterface;
  use Drupal\Core\StringTranslation\StringTranslationTrait;
  use Drupal\paragraphs\Entity\Paragraph;
  use Drupal\paragraphs\ParagraphInterface;
  use Symfony\Component\DependencyInjection\ContainerInterface;
  use Symfony\Component\HttpFoundation\JsonResponse;

  /**
   * Class Address
   *
   * @package Drupal\rosreklama_api\Controller
   */
  class Address extends ControllerBase {

    use StringTranslationTrait;

    /**
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * @var \Drupal\Core\Database\Connection
     */
    protected $database;

    public function __construct(EntityTypeManagerInterface $entityTypeManager, Connection $connection) {
      $this->entityTypeManager = $entityTypeManager;
      $this->database = $connection;
    }

    public static function create(ContainerInterface $container) {
      return new static(
        $container->get('entity_type.manager'),
        $container->get('database')
      );
    }

    public function edit() {
      $token = \Drupal::request()->request->get('token');
      $box_id = \Drupal::request()->request->get('box_id');
      $new_box = \Drupal::request()->request->get('new_box');

      \Drupal::logger('Objects')->notice('token:' . $token);
      $courier_id = LoginApiController::mc_decrypt($token);
      if( !$token && !$courier_id) {
        return new JsonResponse([
          'result' => 0,
          'message' => $this->t('Tokens do not match.'),
        ], 200, ['Content-Type' => 'application/json']);
      }

      if (!$box_id || !$new_box) {
        return new JsonResponse([
          'result' => 0,
          'message' => $this->t('One of required parameter is missing. Box_id: ' . $box_id . ', new_box: ' . $new_box),
        ], 200, ['Content-Type'=> 'application/json']);
      }

      return $this->address_edit($courier_id, $box_id, $new_box);
    }

    public function address_edit($courier_id, $box_id, $new_box) {
      \Drupal::logger('Objects')->notice('Account_id:' . $courier_id);

      if ($courier_id) {
        $query = $this->database->select('paragraph__field_market_box_id', 'box');
        $query->condition('box.field_market_box_id_value', $box_id);
        $query->addField('box', 'entity_id');

        $paragraph_id = $query->execute()->fetchField();

        $paragraph = Paragraph::load($paragraph_id);
        if (!$paragraph) {
          return new JsonResponse([
            'result' => 0,
            'message' => $this->t('Can not load box with @id: ', [
              '@id' => $box_id,
            ]),
          ], 200, ['Content-Type'=> 'application/json']);
        }

        $query = $this->database->select('paragraph__field_market_box_id', 'box');
        $query->condition('box.field_market_box_id_value', $new_box);
        $query->addField('box', 'entity_id');
        $new_box_exist = $query->execute()->fetchField();

        if ($new_box_exist) {
          return new JsonResponse([
            'result' => 0,
            'message' => $this->t('Box with id: @id already exist ', [
              '@id' => $new_box,
            ]),
          ], 200, ['Content-Type'=> 'application/json']);
        }

        if ($paragraph instanceof ParagraphInterface) {
          try {
            $paragraph->set('field_market_box_id', $new_box);
            $paragraph->save();

            return new JsonResponse([
              'result' => 1,
              'message' => $this->t('Successfully update box id for box with @id new box id @new_box', [
                '@id' => $box_id,
                '@new_box' => $new_box,
              ]),
            ], 200, ['Content-Type' => 'application/json']);
          } catch (\Exception $e) {
            watchdog_exception('Error when update box id', $e);
            return new JsonResponse([
              'result' => 0,
              'message' => $this->t('Error when update box id for box with id @id, cannot set new box id @new_box ', [
                '@id' => $box_id,
                '@new_box' => $new_box,
              ]),
            ], 200, ['Content-Type' => 'application/json']);
          }
        }
      }
      else {
        return new JsonResponse([
          'result' => 0,
          'message' => $this->t('Coruier with id @id does not exist.', [
            '@id' => $courier_id,
          ]),
        ], 200, ['Content-Type'=> 'application/json']);
      }
    }

  }
