<?php

/**
  * @yugrinov.ru
  */

namespace Drupal\rosreklama_tools\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the Example module.
 */
class RosreklamaToolsController extends ControllerBase {
  /**
   * Returns a empty front page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function home() {
    $element = array(
      '#markup' => '',
    );
    return $element;
  }
}
