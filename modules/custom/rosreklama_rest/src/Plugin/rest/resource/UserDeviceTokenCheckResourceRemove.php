<?php

  namespace Drupal\rosreklama_rest\Plugin\rest\resource;

  use Drupal\Core\Entity\EntityTypeManagerInterface;
  use Drupal\Core\Session\AccountProxyInterface;
  use Drupal\rest\Plugin\ResourceBase;
  use Drupal\rest\ResourceResponse;
  use Drupal\user\UserInterface;
  use Psr\Log\LoggerInterface;
  use Symfony\Component\DependencyInjection\ContainerInterface;
  use Symfony\Component\HttpFoundation\RequestStack;

  /**
   * Provides a resource to get view modes by entity and bundle.
   *
   * @RestResource(
   *   id = "check_token_from_field_resource",
   *   label = @Translation("check_token_from_field_resource"),
   *   uri_paths = {
   *     "canonical" = "/api/v1/check_token_from_field_resource"
   *   }
   * )
   */
  class UserDeviceTokenCheckResourceRemove extends ResourceBase {

    /**
     * A current user instance.
     *
     * @var \Drupal\Core\Session\AccountProxyInterface
     */
    protected $currentUser;

    /**
     * @var \Symfony\Component\HttpFoundation\Request|null
     */
    protected $request;

    /**
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * UserDeviceTokenCheckResource constructor.
     *
     * @param array $configuration
     * @param $plugin_id
     * @param $plugin_definition
     * @param array $serializer_formats
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Drupal\Core\Session\AccountProxyInterface $current_user
     * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user, RequestStack $request_stack, EntityTypeManagerInterface $entityTypeManager) {
      parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

      $this->currentUser = $current_user;
      $this->request = $request_stack->getCurrentRequest();
      $this->entityTypeManager = $entityTypeManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
      return new static(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $container->getParameter('serializer.formats'),
        $container->get('logger.factory')->get('rosreklama_rest'),
        $container->get('current_user'),
        $container->get('request_stack'),
        $container->get('entity_type.manager')
      );
    }

    /**
     * Responds to GET requests.
     */
    public function get() {
      $query = $this->request->query;

      if (!$query->has('email') || !$query->has('device_token')) {
        return new ResourceResponse([
          'result' => 0,
          'error' => 'One of the required parameters is missing',
        ]);
      }

      $result = $this->entityTypeManager
        ->getStorage('user')
        ->loadByProperties([
          'mail' => $query->get('email'),
          'field_device_id' => $query->get('device_token'),
        ]);

      $user = reset($result);
dsm($user);
return [];
      if ($user instanceof UserInterface) {
        return new ResourceResponse([
          'result' => 1,
          'error' => '',
        ]);
      }
      else {
        return new ResourceResponse([
          'result' => 0,
          'error' => 'User not found',
        ]);
      }
    }

  }
