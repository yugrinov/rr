<?php

/**
 * @yugrinov.ru
 */

namespace Drupal\rosreklama_rest\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get market place node.
 *
 * @RestResource(
 *   id = "get_service_place_resource",
 *   label = @Translation("Get service place"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/service-place/get"
 *   }
 * )
 */
class GetServicePlaceResource extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new GetArticleResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   */
  public function get() {
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    $titles = [
      0 => t('Sunday'),
      1 => t('Monday'),
      2 => t('Tuesday'),
      3 => t('Wednesday'),
      4 => t('Thursday'),
      5 => t('Friday'),
      6 => t('Saturday'),
    ];

    // Get market places.
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $entities = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties([
          'type' => 'service_place',
          'status' => 1,
          'langcode' => $language,
      ]);

    $data = [];

    if (!empty($entities)) {
      $connection = \Drupal::database();
      foreach ($entities as $service_place) {
        // Working time, image, place, etc.
        $paragraph_reference = $service_place->field_working_time->getValue();
        $working_time = [];
        foreach ($paragraph_reference as $reference) {
          $paragraph = \Drupal\paragraphs\Entity\Paragraph::load($reference['target_id']);
          $work_time = $paragraph->field_work_time->getValue();
          $work_day = $paragraph->field_day_of_week->value;
          $working_time[] = [
            'title' => $titles[$work_day],
            'from' => date('H:i', $work_time[0]['from'] - 14400), // 14400 astrakhan timezone offset
            'to'   => date('H:i', $work_time[0]['to'] - 14400), // 14400 astrakhan timezone offset
          ];
        }

        // Geo data.
        $geo = $service_place->get('field_market_place_address')->first()->getValue();
        $coords = json_decode($geo['placemarks']);
        $coords = $coords[0]->coords;

        // Type of services.
        $terms = $service_place->field_service->getValue();
        $services = [];
        if (!empty($terms)) {
          foreach ($terms as $term) {
            $services[] = $connection->query('SELECT name FROM {taxonomy_term_field_data} WHERE tid = :tid', [':tid' => $term['target_id']])->fetchField();
          }
        }

        // Output data.
        $data[$service_place->id()] = [
          'address_geo'    => $coords,
          'address_string' => $service_place->field_market_place_address_text->value,
          'book_data'      => [],
          'id'             => $service_place->id(),
          'working_time'   => $working_time,
          'services'       => $services,
          'updated'        => date('d.m.Y H:i', $service_place->changed->value),
        ];
      }
    }

    if (!empty($data)) {
      return new ResourceResponse($data);
    }
    else {
      $response['message'] = 'No found any service places.';
      return new ResourceResponse($response, 400);
    }
  }
}
