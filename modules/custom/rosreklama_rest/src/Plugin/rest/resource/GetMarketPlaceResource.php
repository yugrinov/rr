<?php

/**
 * @yugrinov.ru
 */

namespace Drupal\rosreklama_rest\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get market place node.
 *
 * @RestResource(
 *   id = "get_market_place_resource",
 *   label = @Translation("Get market place"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/market-place/get"
 *   }
 * )
 */
class GetMarketPlaceResource extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new GetArticleResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   */
  public function get() {
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    // Get market places.
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $entities = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties([
          'type' => 'market_place',
          'status' => 1,
          'langcode' => $language,
      ]);

    $data = [];
    $place = [
      'entrance'  => t('entrance'),
      'floor'     => t('floor'),
      'structure' => t('structure'),
      'building'  => t('building'),
    ];
    foreach ($entities as $market_place) {
      // Market boxes, image, place, etc.
      $paragraph_reference = $market_place->field_market_place_photodata->getValue();
      $market_boxes = [];
      foreach ($paragraph_reference as $paragraph) {
        $image_url = $image = '';
        $market_box = \Drupal\paragraphs\Entity\Paragraph::load($paragraph['target_id']);
        if (!$market_box->field_market_box_image->isEmpty()) {
          $image = $market_box->field_market_box_image->getValue();
          $image = \Drupal\file\Entity\File::load($image[0]['target_id']);
          $image_path = $image->getFileUri();
          $image_url = \Drupal\image\Entity\ImageStyle::load('150x150')
            ->buildUrl($image_path);
        }
        $market_boxes[] = [
          'image_url'  => $image_url,
          'image_original' => (!empty($image)) ? $image->url() : '',
          'place_int'  => $market_box->field_market_box_place_int->value,
          'place_text' => $place[$market_box->field_market_box_place_text->value],
        ];
      }
      // Geo data.
      if (!$market_place->field_market_place_address->isEmpty()) {
        $geo = $market_place->get('field_market_place_address')
          ->first()
          ->getValue();
        $coords = json_decode($geo['placemarks']);
        if (is_array($coords)) {
          $coords = $coords[0]->coords;
        }
      }

      // District and base price.
      $term_id = $market_place->field_district->getValue()[0]['target_id'];
      $connection = \Drupal::database();
      $min_price = $connection->query('SELECT field_base_price_value FROM {taxonomy_term__field_base_price} WHERE entity_id = :entity_id', [':entity_id' => $term_id])->fetchField();
      if (empty($min_price)) {
        $ancestors = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadAllParents($term_id);
        foreach ($ancestors as $term) {
          $min_price = $term->field_base_price->value;
          if (!empty($min_price)) {
            break;
          }
        }
      }

      // Output data.
      $data[$market_place->id()] = [
        'address_geo'    => $coords,
        'address_string' => $market_place->field_market_place_address_text->value,
        'book_data'      => [],
        'id'             => $market_place->id(),
        'market_boxes'   => $market_boxes,
        'price_from'     => $min_price,
        'updated'        => date('d.m.Y H:i', $market_place->changed->value),
      ];
    }

    if (!empty($data)) {
      return new ResourceResponse($data);
    }
    else {
      $response['message'] = 'No found any market places.';
      return new ResourceResponse($response, 400);
    }
  }
}
