<?php

/**
 * @yugrinov.ru
 */

namespace Drupal\rosreklama_rest\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to add order line items.
 *
 * @RestResource(
 *   id = "add_order_line_item_resource",
 *   label = @Translation("Add order line items"),
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/order-line-item/add"
 *   }
 * )
 */
class AddOrderLineItemResource extends ResourceBase {
 /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  /**
   * Constructs a new CreateArticleResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST request.
   *
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   */
  public function post(array $data) {
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    try {
      $tempstore = \Drupal::service('user.private_tempstore')->get('rosreklama_rest');
      $lineItems = $tempstore->get('lineItems');

      $marketPlaceId = $data['nodeId'];
      if (empty($lineItems[$marketPlaceId])) {
        $marketPlace = \Drupal\node\Entity\Node::load($marketPlaceId);

        // District and base price.
        $term_id = $marketPlace->field_district->getValue()[0]['target_id'];
        $connection = \Drupal::database();
        $base_price = $connection->query('SELECT field_base_price_value FROM {taxonomy_term__field_base_price} WHERE entity_id = :entity_id', [':entity_id' => $term_id])->fetchField();
        if (empty($base_price)) {
          $ancestors = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadAllParents($term_id);
          foreach ($ancestors as $term) {
            $base_price = $term->field_base_price->value;
            $baseplus_price = $term->field_baseplus_price->value;
            $inclusive_price = $term->field_inclusive_price->value;
            if (!empty($min_price)) {
              break;
            }
          }
        }
        else {
          $baseplus_price = $connection->query('SELECT field_baseplus_price_value FROM {taxonomy_term__field_baseplus_price} WHERE entity_id = :entity_id', [':entity_id' => $term_id])->fetchField();
          $inclusive_price = $connection->query('SELECT field_inclusive_price_value FROM {taxonomy_term__field_inclusive_price} WHERE entity_id = :entity_id', [':entity_id' => $term_id])->fetchField();
        }

        // store to session node array.
        $lineItems[$marketPlaceId] = [
          'address_string' => $marketPlace->field_market_place_address_text->value,
          'prices' => [
            'base_price' => [
              'value' => $base_price,
              'title' => t('base price'),
            ],
            'base_price_plus' => [
              'value' => $baseplus_price,
              'title' => t('base price plus'),
            ],
            'inclusive_price' => [
              'value' => $inclusive_price,
              'title' => t('inclusive price'),
            ],
          ],
        ];

        $tempstore->set('lineItems', $lineItems);
      }

      return new ResourceResponse('');
    } catch (\Exception $e) {
      return new ResourceResponse('Something went wrong during line item creation. Check your data.', 400);
    }
  }

}
