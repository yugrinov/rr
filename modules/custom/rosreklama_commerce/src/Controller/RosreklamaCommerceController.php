<?php

/**
  * @yugrinov.ru
  */

namespace Drupal\rosreklama_commerce\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;

/**
 * Class RosreklamaCommerceController
 *
 * @package Drupal\rosreklama_commerce\Controller
 */
class RosreklamaCommerceController extends ControllerBase {

  /**
   * @param \Drupal\node\NodeInterface $node
   *
   * @return array
   */
  public function checkout(NodeInterface $node) {
//    $lineItems = [];
//    $typeOfPrice = [];
//    $tempstore = \Drupal::service('user.private_tempstore')->get('rosreklama_rest');
//    $lineItemsGet = $tempstore->get('lineItems');
//
//    $nameOfPrice = [
//      'base_price' => t('base price'),
//      'base_price_plus' => t('base price plus'),
//      'inclusive_price' => t('inclusive price'),
//    ];
//
//    foreach ($lineItemsGet as $key => $item) {
//      // Order by type of price.
//      if (!empty($item['type_of_price'])) {
//        $lineItems[$item['type_of_price']][$key] = $item;
//        $typeOfPrice[$item['type_of_price']] = $nameOfPrice[$item['type_of_price']];
//      }
//
//      // Amount per line and per price type.
//      $lineItemPrice = $item['prices'][$item['type_of_price']]['value'];
//      $countPeriods = ($item['booking_data']['end'] - $item['booking_data']['start']) / 1209600;
//      $lineItems[$item['type_of_price']][$key]['amount'] = [
//        'periods' => $countPeriods,
//        'price_per_period' => $lineItemPrice,
//      ];
//
//      // Booking date.
//      if (!empty($item['booking_data']['start'])) {
//        $lineItems[$item['type_of_price']][$key]['booking_data']['formatted_start'] = date('d.m.Y', $item['booking_data']['start']);
//      }
//      if (!empty($item['booking_data']['end'])) {
//        $lineItems[$item['type_of_price']][$key]['booking_data']['formatted_end'] = date('d.m.Y', $item['booking_data']['end']);
//      }
//    }
//
//    // Total amount.
//    $resultPrice = [];
//    foreach ($lineItems as $key => $items) {
//      foreach ($items as $nid => $item) {
//        $resultPrice[$key] = [
//          'total_amount' => count($items) * $item['amount']['periods'] * $item['amount']['price_per_period'],
//          'title' => $nameOfPrice[$key],
//        ];
//      }
//    }
//    $total = 0;
//    foreach ($resultPrice as $key => $price) {
//      $total += $price['total_amount'];
//    }
//    $resultPrice['total'] = $total;
//
//    $userBalance = ['balance' => 0];
//    // User balance.
//    $tempstore = \Drupal::service('user.private_tempstore')->get('rosreklama_rest');
//    $userBalanceGet = $tempstore->get('userBalance');
//    if (!empty($userBalanceGet)) {
//      $userBalance['balance'] = $userBalanceGet;
//    }

    $order = \Drupal::entityTypeManager()
      ->getViewBuilder('node')
      ->view($node);
    return [
      '#markup' => render($order),
      '#cache' => ['max-age' => 0],
    ];

//    return array(
//      '#theme' => 'checkout_page',
//      '#line_items' => $lineItems,
//      '#type_of_prices' => $typeOfPrice,
//      '#total_amounts' => $resultPrice,
//      '#user_balance' => $userBalance,
//      '#cache' => array('max-age' => 0),
//    );
  }

  /**
   * @return array
   */
  public function complete() {
    $tempstore = \Drupal::service('user.private_tempstore')->get('rosreklama_rest');
    $lineItemsGet = $tempstore->get('lineItems');

    if (!empty($lineItemsGet)) {
      $totalAmount = 0;
      foreach ($lineItemsGet as $nid => $lineItem) {
        $typeOfPrice = $lineItemsGet[$nid]['type_of_price'];
        $lineItemPrice = $lineItemsGet[$nid]['prices'][$typeOfPrice]['value'];
        $countPeriods = ($lineItem['booking_data']['end'] - $lineItem['booking_data']['start']) / 1209600;
        $totalAmount += $lineItemPrice * $countPeriods;
      }
    }

    $tempstore = \Drupal::service('user.private_tempstore')->get('rosreklama_rest');
    $userBalanceGet = $tempstore->get('userBalance');

    $status = 'error';

    if ($totalAmount <= $userBalanceGet) {
      $status = 'complete';
      $userBalance = $userBalanceGet - $totalAmount;
      $tempstore->set('userBalance', $userBalance);
      $tempstore->set('lineItems', []);
    }

    return array(
      '#theme' => 'checkout_complete_page',
      '#status' => $status,
      '#cache' => array('max-age' => 0),
    );
  }
}
