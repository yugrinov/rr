<?php

namespace Drupal\rosreklama_maps\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the rosreklama maps module.
 */
class MapsPage extends ControllerBase {
  /**
   * Returns a map page.
   *
   * @return array
   */

  public function build() {
    return [
      '#markup' => '',
    ];
  }
}
