<?php
/**
 * @file
 */

namespace Drupal\rosreklama_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 *
 * @Block(
 *   id = "rosreklama_cart_line_items",
 *   admin_label = @Translation("Line items in cart"),
 * )
 */
class CartLineItems extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('\Drupal\rosreklama_blocks\Form\CartLineItemsForm');
    return $form;
  }

  // public function build() {
  //   $tempstore = \Drupal::service('user.private_tempstore')->get('rosreklama_rest');
  //   $lineItems = $tempstore->get('lineItems');

  //   if (!empty($lineItems)) {
  //     return array(
  //       '#theme' => 'cart_line_items',
  //       '#line_items' => $lineItems,
  //       '#cache' => array('max-age' => 0),
  //     );
  //   }

  //   return ['#markup' => '', '#cache' => array('max-age' => 0)];
  // }
}
