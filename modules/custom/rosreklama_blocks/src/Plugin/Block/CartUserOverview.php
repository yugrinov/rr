<?php
/**
 * @file
 */

namespace Drupal\rosreklama_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 *
 * @Block(
 *   id = "rosreklama_cart_user_overview",
 *   admin_label = @Translation("User overview in cart"),
 * )
 */
class CartUserOverview extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = '';

    $currentUser = \Drupal::currentUser()->id();
    if ($currentUser > 0) {
      $account = \Drupal\user\Entity\User::load($currentUser);
      $user_fio = $account->name->value;
      if (!empty($account->field_user_fio)) {
        $user_fio = $account->field_user_fio->value;
      }
      $build = '<div class="row"><div class="col-sm-8"><a href="/user"><i class="far fa-user"></i>' . $user_fio . '</a></div><div class="col-sm-4 text-right"><a href="/user/logout">Выйти</a></div></div>';
    }

    return ['#markup' => $build, '#cache' => array('max-age' => 0)];
  }
}
