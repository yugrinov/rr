<?php
/**
 * @file
 */

namespace Drupal\rosreklama_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 *
 * @Block(
 *   id = "rosreklama_header_left",
 *   admin_label = @Translation("Header Left Block"),
 * )
 */
class HeaderLeftBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return [
        '#markup' => '<a href="/user/login">' . $this->t('Log in') . '</a>',
      ];
    }

    return [
      '#markup' => '<a href="/user">' . $this->t('Account') . '</a>',
    ];
  }
}
