<?php
/**
 * @file
 */

namespace Drupal\rosreklama_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 *
 * @Block(
 *   id = "rosreklama_map",
 *   admin_label = @Translation("Map block"),
 * )
 */
class MapBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<div id="homemap" style="width: 100%; height: 400px"></div>',
    ];
  }
}
