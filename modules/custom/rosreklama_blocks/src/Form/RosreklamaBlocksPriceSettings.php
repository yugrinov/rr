<?php

  namespace Drupal\rosreklama_blocks\Form;

  use Drupal\Core\Form\ConfigFormBase;
  use Drupal\Core\Form\FormStateInterface;

  /**
   * Class RosreklamaBlocksPriceSettings
   *
   * @package Drupal\rosreklama_blocks\Form
   */
  class RosreklamaBlocksPriceSettings extends ConfigFormBase {

    /**
     * @return string
     */
    public function getFormId() {
      return 'rosreklama_blocks_price_settings';
    }

    /**
     * @return array
     */
    protected function getEditableConfigNames() {
      return ['rosreklama.prices'];
    }

    /**
     * @param array $form
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *
     * @return array
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
      $form['base'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Base'),
        '#default_value' => $this->config('rosreklama.prices')->get('base'),
      ];

      $form['base_plus'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Base+'),
        '#default_value' => $this->config('rosreklama.prices')->get('base_plus'),
      ];

      $form['all_inclusive'] = [
        '#type' => 'textfield',
        '#title' => $this->t('All inclusive'),
        '#default_value' => $this->config('rosreklama.prices')->get('all_inclusive'),
      ];

      return parent::buildForm($form, $form_state);
    }

    /**
     * @param array $form
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
      parent::validateForm($form, $form_state);
    }

    /**
     * @param array $form
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      $this->config('rosreklama.prices')
        ->set('base', $form_state->getValue('base'))
        ->set('base_plus', $form_state->getValue('base_plus'))
        ->set('all_inclusive', $form_state->getValue('all_inclusive'))
        ->save();
      parent::submitForm($form, $form_state);
    }

  }

