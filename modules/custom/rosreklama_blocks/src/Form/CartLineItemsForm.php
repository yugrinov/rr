<?php

namespace Drupal\rosreklama_blocks\Form;

use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use \Drupal\node\Entity\Node;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\user\Entity\User;

/**
 * Class CartLineItemsForm
 *
 * @package Drupal\rosreklama_blocks\Form
 */
class CartLineItemsForm extends FormBase {

  /**
   * @return string
   */
  public function getFormId() {
    return 'cart_line_items';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['#prefix'] = '<div id="cart-line-items__form-wrapper">';
    $form['#suffix'] = '</div>';
    $tempstore = \Drupal::service('user.private_tempstore')->get('rosreklama_rest');
    $lineItemsGet = $tempstore->get('lineItems');
    $lineItems = [];

    // Check that lineitems contains only marketplaces with equal price.
    if (!empty($lineItemsGet)) {
      foreach ($lineItemsGet as $key => $item) {
        $base_price = $item['prices']['base_price']['value'];
        $base_price_plus = $item['prices']['base_price_plus']['value'];
        $inclusive_price = $item['prices']['inclusive_price']['value'];
        $price_string = $base_price . '_' . $base_price_plus . '_' . $inclusive_price;
        $lineItems[$price_string][$key] = $item;
      }
    }

    if (!empty($lineItems)) {
      $prices = \Drupal::config('rosreklama.prices');

      $form['top'] = [
        '#markup' => '<div class="cart-line-items filled"><div class="cart-line-items__title">' . t('Your order') . '</div>',
      ];
      foreach ($lineItems as $price_string => $arr) {
        $countLineItems = count($lineItems[$price_string]);
        $form['line_item_' . $price_string . '_wrapper_start'] = [
          '#markup' => '<div class="cart-line-items__line-items">',
        ];
        foreach ($arr as $nid => $lineItem) {
          $form['line_item_' . $nid . '_wrapper_start'] = [
            '#markup' => '<div class="cart-line-items__line-item" data-line-item="' . $nid . '">',
          ];
          $form['line_item_' . $nid . '_address'] = [
            '#markup' => '<div class="cart-line-items__line-item-title">' . $lineItem['address_string'] . '</div>',
          ];
          $form['line_item_' . $nid . '_remove'] = [
            '#type'   => 'checkbox',
            '#title'  => t('Remove'),
            '#prefix' => '<div class="remove" data-line-item="' . $nid . '">',
            '#suffix' => '</div>',
          ];
          $form['line_item_' . $nid . '_book_date'] = [
            '#type' => 'fieldset',
            '#title' => t('Book dates'),
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
          ];
          $form['line_item_' . $nid . '_book_date']['book_' . $nid . '_start'] = [
            '#type'              => 'textfield',
            '#title'             => t('Start book'),
            '#placeholder'       => t('Start book'),
            '#default_value'     => !(empty($lineItemsGet[$nid]['booking_data']['start'])) ? date('d.m.Y', $lineItemsGet[$nid]['booking_data']['start']) : '',
            '#prefix'            => '<div class="input-daterange input-group" id="datepicker">',
          ];
          $form['line_item_' . $nid . '_book_date']['book_' . $nid . '_end'] = [
            '#type'              => 'textfield',
            '#title'             => t('End book'),
            '#placeholder'       => t('End book'),
            '#default_value'     => !(empty($lineItemsGet[$nid]['booking_data']['end'])) ? date('d.m.Y', $lineItemsGet[$nid]['booking_data']['end']) : '',
            '#suffix'            => '</div>',
          ];
          $form['book_description_' . $nid] = [
            '#markup' => '<div class="book-description"></div>',
          ];
          $form['line_item_' . $nid . '_wrapper_end'] = [
            '#markup' => '</div>',
          ];
        }

      }
      $form['message'] = [
        '#markup' => '<div class="result_message"></div>',
      ];
      $form['prices'] = [
        '#type' => 'radios',
        '#title' => 'Тарифный план',
        '#options' => [
          'base' => 'Базовый (' . $prices->get('base') . 'р.)',
          'base_plus' => 'Базовый+ (' . $prices->get('base_plus') . 'р.)',
          'all_inclusive' => 'Всё включено (' . $prices->get('all_inclusive') . 'р.)',
        ],
      ];
      $form['actions'] = [
        '#type'  => 'button',
        '#value' => $this->t('Place order'),
        '#ajax'  => [
          'callback' => '::setMessage',
        ],
      ];
      $form['#bottom'] = [
        '#markup' => '</div>',
      ];
    }
    $form['refresh_button'] = [
      '#type'   => 'submit',
      '#name'   => 'refresh',
      '#value'  => t('Refresh'),
      '#submit' => [[$this, 'refreshSubmit']],
      '#ajax'   => [
        'callback' => [$this, 'refreshCallback'],
        'wrapper'  => 'cart-line-items__form-wrapper',
        'effect'   => 'fade',
      ],
      '#prefix' => '<div class="refresh hidden">',
      '#suffix' => '<div>',
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setMessage(array $form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $response = new AjaxResponse();
    $message = '';

    if (empty($values['prices'])) {
      $message = 'Поле тарифный план обязательно для заполнения.';
    }
    if (\Drupal::currentUser()->isAnonymous()) {
      $message = 'Войдите или зарегестрируйтесь, чтобы продолжить оформление.';
    }
    if (!empty($message)) {
      $response->addCommand(new HtmlCommand('.result_message', '<div class="alert alert-primary" role="alert">' . $message . '</div>'));
    }
    else {
      // collect dates and nids.
      $values = $form_state->getValues();
      $booking_data = [];
      foreach ($values as $string => $value) {
        if (strpos($string, '_start') !== FALSE) {
          $nid = preg_replace('/[^0-9]/', '', $string);
          $booking_data[$nid] = [
            'start' => $values[$string],
          ];
        }
        if (strpos($string, '_end') !== FALSE) {
          $nid = preg_replace('/[^0-9]/', '', $string);
          $booking_data[$nid]['end'] = $values[$string];
        }
      }

      if (!empty($booking_data)) {
        $prices = \Drupal::config('rosreklama.prices');
        $price = $prices->get($values['prices']);
        foreach ($booking_data as $nid => $dates) {
          $from = new \DateTime($dates['start']);
          $to = new \DateTime($dates['end']);
          $interval = $from->diff($to);
          $booking_data[$nid]['weeks'] = $interval->days / 7;
        }

        $node = Node::create([
         'field_order_customer' => User::load(\Drupal::currentUser()->id()),
          'field_order_note' => '',
          'field_status' => 'created',
          'type' => 'order',
          'field_price_type' => $values['prices'],
        ]);
        $node->save();

        $total = 0;
        foreach ($booking_data as $nid => $dates) {
          $from = strtotime($dates['start'] . '00:00:01');
          $to = strtotime($dates['end'] . '23:59:59');
          $paragraph = Paragraph::create([
            'type' => 'order_line_item',
            'field_prgrphs_order_line_item' => [
              'target_id' => $nid,
              'target_revision_id' => $nid,
            ],
            'field_prgrphs_line_item_date' => [
              0 => [
                'value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $from),
                'end_value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $to),
              ]
            ],
            'field_prgrphs_total' => $price * $dates['weeks'],
          ]);

          $total += $price * $dates['weeks'];
          $paragraph->save();
          $node->field_order_line_item->appendItem($paragraph);
        }

        $node->field_order_total = $total;
        $node->save();
      }

      // Если нет, то обновляем lineItems и редиректим на страницу оформления заказа.
//      $url = Url::fromRoute('rosreklama_commerce.checkout');
      $this->messenger()->addMessage('Ваша заявка принята.', 'status');
      $command = new RedirectCommand('/checkout/' . $node->id());
      $response->addCommand($command);
    }

    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function refreshSubmit(array $form, FormStateInterface $form_state) {
     // $form_state->setRebuild(TRUE);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function refreshCallback(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }
}
