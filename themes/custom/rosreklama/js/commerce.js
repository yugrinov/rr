var ipay = new IPAY({
  api_token: '7j9m4nhb4pba1fgbehe2kee11k'
});

(function ($, Drupal) {
  Drupal.behaviors.rosreklamaCommerceBehavior = {
    attach: function (context, settings) {
      // Remove line items.
      $('.remove i[data-line-item-process="remove"]').once('removeLineItem').on('click', function() {
        // Remove from order.
        var lineItemID = $(this).closest('.remove').attr('data-line-item');
        let sessionToken = new Promise((resolve, reject) => {
          $.ajax({
            url: '/rest/session/token',
            dataType: 'text',
            type: 'GET',
            success: response => {
              resolve(response);
            },
            error: response => {
              reject(response);
            },
          });
        });

        sessionToken.then(
          token => {
            $.ajax({
              url: '/api/v1/order-line-item/remove',
              type: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': token,
              },
              dataType: 'json',
              data: JSON.stringify({
                lineItemID: lineItemID,
              }),
              success: response => {
                $('.refresh .form-submit').mousedown();
              },
              error: response => {
                alert('ERROR');
              },
            });
          },
        );
        $('.refresh .form-submit').mousedown();
      })
      // Label click.
      $('.book-prices label').on('click', function() {
        $(this).closest('.book-prices').find('label').removeClass('active');
        $(this).addClass('active');
      })

      // Datepicker.
        // Get current date.
        var currentDate = new Date();
        currentDate.setMonth(currentDate.getMonth() + 6); // 6 - 6 months
        var day = ('0' + currentDate.getDate()).slice(-2);
        var month = ('0' + (currentDate.getMonth()+1)).slice(-2);
        var year = currentDate.getFullYear();

        $('#cart-line-items__form-wrapper .input-daterange').once('addDatepicker').datepicker({
            format: 'dd.mm.yyyy',
            startDate: 'now',
            endDate: day + '.' + month + '.' + year,
            language: 'ru',
            autoclose: true,
            daysOfWeekDisabled: [0, 1, 2, 3, 4, 6],
            daysOfWeekHighlighted: [5]
        });

        $('#cart-line-items__form-wrapper .input-daterange .input-group-addon').click('on', function() {
          $(this).prev().datepicker('show');
        })

        $('.input-daterange .element-wrapper:first-child input').once('changeStartDate').datepicker().on('changeDate', function(e) {
          $(this).closest('.input-daterange').find('.element-wrapper').last().show();
          var endElement = $(this).closest('.input-daterange').find('.element-wrapper').last().find('input');
          // Disable days of week for booking.
          var days = [0, 1, 2, 3, 4, 5, 6];
          var currentDay = e.date.getDay();
          delete days[currentDay];
          endElement.datepicker('clearDates');
          endElement.datepicker('setDaysOfWeekDisabled', days);

          // Enable book only for period in 14 days.
          var nextWeek = new Date();
          nextWeek.setTime(e.date.getTime() + 604800000);
          day = ('0' + nextWeek.getDate()).slice(-2);
          month = ('0' + (nextWeek.getMonth()+1)).slice(-2);
          year = nextWeek.getFullYear();
          var datesDisabled = [day + '.' + month + '.' + year];

          while (nextWeek.getTime() < currentDate.getTime()) {
            day = ('0' + nextWeek.getDate()).slice(-2);
            month = ('0' + (nextWeek.getMonth()+1)).slice(-2);
            year = nextWeek.getFullYear();
            datesDisabled.push(day + '.' + month + '.' + year);
            nextWeek.setTime(nextWeek.getTime() + 1209600000);
          }
          endElement.datepicker('setDatesDisabled', datesDisabled);

          // Set end booking date.
          var endDate = new Date();
          endDate.setTime(e.date.getTime() + 1209600000);
          day = ('0' + endDate.getDate()).slice(-2);
          month = ('0' + (endDate.getMonth()+1)).slice(-2);
          year = endDate.getFullYear();
          endElement.datepicker('setDate', day + '-' + month + '-' + year);

          // Get end date and calculate total amount.
          $('.input-daterange .element-wrapper:last-child input').once('changeEndDate').datepicker().on('changeDate', function(e) {
            if (e.date instanceof Date){
              var nid = parseInt($(this).attr('data-drupal-selector').replace(/[^\d]+/g, ''));

              var startPicker = $(this).closest('.input-daterange').find('.element-wrapper').first().find('input').datepicker('getDate');
              var startDate = new Date(startPicker);
              var periodOfBook = (e.date.getTime() - startDate.getTime()) / 1209600000;

              let sessionToken = new Promise((resolve, reject) => {
                $.ajax({
                  url: '/rest/session/token',
                  dataType: 'text',
                  type: 'GET',
                  success: response => {
                    resolve(response);
                  },
                  error: response => {
                    reject(response);
                  },
                });
              });

              sessionToken.then(
                token => {
                  $.ajax({
                    url: '/api/v1/order-line-item/edit',
                    type: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                      'X-CSRF-Token': token,
                    },
                    dataType: 'json',
                    data: JSON.stringify({
                      nid: nid,
                      startBook: startDate.getTime(),
                      endBook: e.date.getTime(),
                      timeZone: e.date.getTimezoneOffset(),
                    }),
                    success: response => {
                      // $('.refresh .form-submit').mousedown();
                    },
                    error: response => {
                      alert('ERROR');
                    },
                  });
                },
              );

              $(this).closest('.cart-line-items__line-items').find('.book-prices').removeClass('hidden')
              $(this).closest('.cart-line-items__line-items').find('.book-prices label .amount-calculate').each(function() {
                var amount = parseFloat($(this).attr('data-amount'));
                if ($(this).attr('data-count') > 1) {
                  $(this).data('data-count-selected-' + nid, periodOfBook);

                  var dataSelected = $(this).data();
                  var periodArr = new Array();
                  for (key in dataSelected) {
                    if (key.indexOf('dataCountSelected') !== -1) {
                      periodArr[key] = dataSelected[key];
                    }
                  }
                  var booksPeriod = 0;
                  var selectedItem = 0;
                  for (key in periodArr) {
                    booksPeriod +=  periodArr[key];
                    selectedItem++;
                  }

                  var amountSum = amount * selectedItem * booksPeriod;
                  $(this).text('x ' + selectedItem + ' x периодов: ' + booksPeriod + ' = ' + amountSum + ' руб.');
                }
                else {
                  var amountSum = periodOfBook * amount;
                  $(this).text(' x периодов: ' + periodOfBook + ' = ' + amountSum + ' руб.');
                }
              })
            }
          });
        });

        // Set type of price.
        $('.book-prices label').once('setTypeOfPrice').on('click', function() {
          var attrFor = $(this).attr('for');
          var nids = new Array();
          var typeOfPrice = '';
          $(this).closest('.cart-line-items__line-items').find('.cart-line-items__line-item').each(function() {
            var nid = $(this).attr('data-line-item');;
            nids[nid] = nid;
            if (attrFor.indexOf('base-price') !== -1 && attrFor.indexOf('base-price-plus') === -1) {
              typeOfPrice = 'base_price';
            }
            if (attrFor.indexOf('base-price-plus') !== -1) {
              typeOfPrice = 'base_price_plus';
            }
            if (attrFor.indexOf('inclusive-price') !== -1) {
              typeOfPrice = 'inclusive_price';
            }
          });

          let sessionToken = new Promise((resolve, reject) => {
            $.ajax({
              url: '/rest/session/token',
              dataType: 'text',
              type: 'GET',
              success: response => {
                resolve(response);
              },
              error: response => {
                reject(response);
              },
            });
          });

          sessionToken.then(
            token => {
              $.ajax({
                url: '/api/v1/order-line-item/set-type-of-price',
                type: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'X-CSRF-Token': token,
                },
                dataType: 'json',
                data: JSON.stringify({
                  nids: nids,
                  typeOfPrice: typeOfPrice,
                }),
                success: response => {
                  // $('.refresh .form-submit').mousedown();
                },
                error: response => {
                  alert('ERROR');
                },
              });
            },
          );

        })

      $('.button.add-payment').once('addPayment').on('click', function() {
        var addAmount = parseInt($(this).prev().val());
        if (addAmount > 0) {
          let sessionToken = new Promise((resolve, reject) => {
          $.ajax({
            url: '/rest/session/token',
            dataType: 'text',
            type: 'GET',
              success: response => {
                resolve(response);
              },
              error: response => {
                reject(response);
              },
            });
          });
          sessionToken.then(
            token => {
              $.ajax({
                url: '/api/v1/commerce/add/user-balance',
                type: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'X-CSRF-Token': token,
                },
                dataType: 'json',
                data: JSON.stringify({
                  addAmount: addAmount,
                }),
                success: response => {
                  // Get new Balance.
                  $.ajax({
                    url: Drupal.url('api/v1/commerce/get/user-balance'),
                    type: 'GET',
                    dataType: 'json',
                    data: {
                      '_format': 'json',
                    },
                    success: response => {
                      var userBalance = response.balance;
                      var totalAmount = parseFloat($('.checkout__total-wrapper .total').attr('data-commerce-total'));
                      $('.user-balance .balance').text(userBalance);

                      if (userBalance > totalAmount) {
                        var html = '<button class="button form-submit btn-primary btn proceed-checkout" value="Перейти к оплате">Оформить заказ</button>';
                        $('.checkout__add-payment').html(html);
                        window.location.href = 'checkout/complete';
                      }
                    },
                    error: response => {
                      console.log(response.respononseText);
                    },
                  });
                },
                error: response => {
                  alert('ERROR');
                },
              });
            },
          );
        }
        else {
          $(this).prev().removeClass('hidden');
          $(this).prev().tooltip('show');
        }
      })

      // Checkout complete.
      $('.button.proceed-checkout').once('checkoutRedirect').on('click', function() {
        window.location.href = 'checkout/complete';
      })

      var width = $(window).width();
      if (width < 762) {
        $('.input-daterange.input-group .form-text').prop('readonly', true);
      }
    }
  };

  $(document).ready(function() {

  });
})(jQuery, Drupal);
