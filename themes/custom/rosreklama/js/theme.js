/**
 * @file
 * Theme hooks for the rosreklama theme.
 */
(function ($, Drupal, Rosreklama, Attributes) {
  // Toggle password visibility.
  $('.password-toggle').click(function() {
    if (!$(this).next().hasClass('pass-visible')) {
      $(this).next().addClass('pass-visible');
      $(this).next().find('input').prop('type', 'text');
      $(this).text(Drupal.t('Hide password'));
    }
    else {
      $(this).next().removeClass('pass-visible');
      $(this).next().find('input').prop('type', 'password');
      $(this).text(Drupal.t('Show password'));
    }
    return false;
  })

  // Auto fill the username field on registration form.
  $('.user-register-form.user-form button').on('click', function() {
    var email = $('.user-register-form').find('input[type="email"]').val();
    $('.user-register-form').find('input[type="text"]').val(email);
  })

  // Cart close-open.
  $('.cart__wrapper i, #block-rosreklama-main-menu i').on('click', function() {
    if ($('body').hasClass('path-map')) {
      $('.cart').toggleClass('close');
    }
    else {
      window.location.href = '/map';
    }
  });

  // Show/hide req call window.
  $('#block-shapkasprava .field--name-body a').on('click', function() {
    $('#block-zakazatzvonok').toggleClass('active');
  });

  $('#block-zakazatzvonok .form-type-processed-text i, a[rel="call"]').on('click', function() {
    $('#block-zakazatzvonok').toggleClass('active');

    return false;
  });

  $('.content.map-block-inside').on('click', function() {
    window.location.href = '/map';
  });

  if ($('.countdown-wrapper').length) {
    var startBuy = new Date();
    startBuy = new Date(startBuy.getFullYear() + 1, 1 - 1, 1);

    $('.countdown-wrapper').countdown({
      until: startBuy,
      format: 'DHM', description: ''
    });
  }

  // $('a.franchise').on('click', function () {
  //   if (!$('.path-frontpage').length) {
  //     window.location.href = '/#francise-scroll';
  //   }
  //   else {
  //     $('html, body').animate({
  //       scrollTop: $('#francise-scroll').offset().top
  //     }, 800);
  //   }
  //
  //   return false;
  // });

})(window.jQuery, window.Drupal, window.Drupal.bootstrap, window.Attributes);
