(function ($, Drupal) {
  Drupal.behaviors.rosreklamaConstructorBehavior = {
    attach: function (context, settings) {
      // Set type of price.
      $('.design-preview img').once('setDesign').on('click', function() {
        var imgSrc = $(this).attr('src');
        $('.design-edit').css('background-image', 'url(' + imgSrc + ')');
        $('.design-edit').removeClass('hidden');
        $('.design-edit *[data-trigger="manual"]').tooltip('show');
      });

      $('.design-edit input, .design-edit textarea').once('removeTooltip').on('focus', function() {
        $(this).tooltip('hide');
      })

    }
  }
})(jQuery, Drupal);
