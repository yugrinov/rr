(function ($, Drupal) {
  Drupal.behaviors.rosreklamaBehavior = {
    attach: function (context, settings) {
    }
  };

  $(document).ready(function() {
    // Main map.
    if ($('#homemap').length) {
      // Get json and create map with baloons.
      ymaps.ready(function() {
        var mapCenter = [48.048659030309416, 46.333622169013104],
        homeMap = new ymaps.Map('homemap', {
          center: mapCenter,
          zoom: 11
        }),
        placemarks = [];
        // Custom layout for baloon.
        var customBalloonContentLayout = ymaps.templateLayoutFactory.createClass([
          '{% for geoObject in properties.geoObjects %}',
            '{{ geoObject.properties.balloonContentHeader|raw }}',
            '{{ geoObject.properties.balloonContentBody|raw }}',
          '{% endfor %}'
        ].join(''));

        $('#homemap').on('click', function() {
          $('.cart').addClass('close');
        });

        $('#homemap').on('click', '.baloon-footer__book', function() {
          var nodeId = $(this).data().nodeId;
          let sessionToken = new Promise((resolve, reject) => {
          $.ajax({
            url: '/rest/session/token',
            dataType: 'text',
            type: 'GET',
              success: response => {
                resolve(response);
              },
              error: response => {
                reject(response);
              },
            });
          });

          sessionToken.then(
            token => {
              $.ajax({
                url: '/api/v1/order-line-item/add',
                type: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'X-CSRF-Token': token,
                },
                dataType: 'json',
                data: JSON.stringify({
                  nodeId: nodeId,
                }),
                success: response => {
                  $('.cart').removeClass('close');
                  $('.refresh .form-submit').mousedown();
                },
                error: response => {
                  alert('ERROR');
                },
              });
            },
          );
        });

        var clusterer = new ymaps.Clusterer({
          clusterDisableClickZoom: true,
          clusterOpenBalloonOnClick: true,
          clusterBalloonPanelMaxMapArea: 0,
          clusterBalloonMaxHeight: 200,
          clusterBalloonContentLayout: customBalloonContentLayout
        });

        $.ajax({
          url: Drupal.url('api/v1/market-place/get'),
          type: 'GET',
          dataType: 'json',
          data: {
            '_format': 'json'
          },
          success: response => {
            $.each(response, function(index, item) {
              var marketBoxes = '';
              // var colWidth = Math.ceil(12 / item['market_boxes'].length);

              $.each(item['market_boxes'], function(ind, row) {
                marketBoxes +=  '<div class="col-sm-4 col-xs-2">' +
                                  '<a class="" href="' + row['image_original'] +'" data-fancybox="gallery-' + index + '">' +
                                    '<img src="' + row['image_url'] + '" class="img-fluid img-thumbnail" alt="" title="" />' +
                                  '</a>' +
                                  '<div class="baloon-body__image-info">' + row['place_text'] + ': ' + row['place_int'] + '</div>' +
                                '</div>';
              });

              var placemarkId = parseInt(item.id);

              if (item['address_geo'] !== null) {
                var placemark = new ymaps.Placemark([item['address_geo'][1], item['address_geo'][0]], {
                  balloonContentHeader: '<div class="baloon-header">' + item['address_string'] + '</div>',
                  balloonContentBody: '<div class="row baloon-body">' +
                    '<div class="row">' + marketBoxes + '</div>' +
                    '</div>' +
                    '<div class="baloon-footer">' +
                    '<button class="baloon-footer__book btn btn-success" data-node-id="' + item['id'] + '">Добавить в заказ</button>' +
                    '<br/> Стоимость от: 198 руб. <br/> Информация обновлена: ' + item['updated'] +
                    '</div>',
                  placemarkId: placemarkId
                });

                placemarks.push(placemark);
              }
            });
            clusterer.add(placemarks);
            homeMap.geoObjects.add(clusterer);
            $('a[data-fancybox="gallery"]').fancybox({
              toolbar  : false,
              smallBtn : true,
            });
          },
          error: response => {
            console.log(response.respononseText);
          },
        });
      });
    }

    // Base price checout map.
    if ($('#service-place-map').length) {
      // Get json and create map with baloons.
      ymaps.ready(function() {
        var mapCenter = [48.048659030309416, 46.333622169013104],
        homeMap = new ymaps.Map('service-place-map', {
          center: mapCenter,
          zoom: 11
        }),
        placemarks = [];
        // Custom layout for baloon.
        var customBalloonContentLayout = ymaps.templateLayoutFactory.createClass([
          '{% for geoObject in properties.geoObjects %}',
            '{{ geoObject.properties.balloonContentHeader|raw }}',
            '{{ geoObject.properties.balloonContentBody|raw }}',
          '{% endfor %}'
        ].join(''));

        $('#homemap').on('click', function() {
          $('.cart').addClass('close');
        });

        // $('#homemap').on('click', '.baloon-footer__book', function() {
        //   var nodeId = $(this).data().nodeId;
        //   let sessionToken = new Promise((resolve, reject) => {
        //   $.ajax({
        //     url: '/rest/session/token',
        //     dataType: 'text',
        //     type: 'GET',
        //       success: response => {
        //         resolve(response);
        //       },
        //       error: response => {
        //         reject(response);
        //       },
        //     });
        //   });

        //   sessionToken.then(
        //     token => {
        //       $.ajax({
        //         url: '/api/v1/order-line-item/add',
        //         type: 'POST',
        //         headers: {
        //           'Content-Type': 'application/json',
        //           'X-CSRF-Token': token,
        //         },
        //         dataType: 'json',
        //         data: JSON.stringify({
        //           nodeId: nodeId,
        //         }),
        //         success: response => {
        //           $('.cart').removeClass('close');
        //           $('.refresh .form-submit').mousedown();
        //         },
        //         error: response => {
        //           alert('ERROR');
        //         },
        //       });
        //     },
        //   );
        // });

        var clusterer = new ymaps.Clusterer({
          clusterDisableClickZoom: true,
          clusterOpenBalloonOnClick: true,
          clusterBalloonPanelMaxMapArea: 0,
          clusterBalloonMaxHeight: 200,
          clusterBalloonContentLayout: customBalloonContentLayout
        });

        $.ajax({
          url: Drupal.url('api/v1/service-place/get'),
          type: 'GET',
          dataType: 'json',
          data: {
            '_format': 'json',
          },
          success: response => {
            $.each(response, function(index, item) {
              var marketBoxes = '<div class="title">Время работы:</div>';
              $.each(item['working_time'], function(ind, row) {
                marketBoxes +=  '<div>' +
                                  row['title'] + ': с ' + row['from'] + ' до ' + row['to'] +
                                '</div>';
              });
              var services = '<div class="title">Услуги:</div>';
              $.each(item['services'], function(ind, row) {
                services +=  '<div class="list">' + row + '</div>';
              });

              var placemarkId = parseInt(item.id);

              var placemark = new ymaps.Placemark([item['address_geo'][1], item['address_geo'][0]], {
                balloonContentHeader: '<div class="baloon-header">' + item['address_string'] + '</div>',
                balloonContentBody: '<div class="row baloon-body services">' +
                                      marketBoxes +
                                      services +
                                    '</div>' +
                                    '<div class="baloon-footer">' +
                                      '<button class="baloon-footer__book btn btn-success" data-node-id="' + item['id'] + '">Выбрать</button>' +
                                    '</div>',
                placemarkId: placemarkId
              });

              placemarks.push(placemark);
            });
            clusterer.add(placemarks);
            homeMap.geoObjects.add(clusterer);
            $('a[data-fancybox="gallery"]').fancybox({
              toolbar  : false,
              smallBtn : true,
            });
          },
          error: response => {
            console.log(response.respononseText);
          },
        });
      });
    }

  });
})(jQuery, Drupal);
